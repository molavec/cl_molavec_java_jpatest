
********************************
Descripción de los paquetes:
********************************
src/main/java
	cl.molavec.entities
		Paquete en el que se encuentran las clases
		
	com.mycila.sandbox.junit.runner
		Paquete para la implementación de la concurrencia de los test JUNIT

---
		
src/test/java
	cl.molavec.compare 
		Test realizados con rutinas de comparación. (analizar el uso de una aplicación 
		real en vez de utilizar Junit ya que tienen otra finalidad)
		
	cl.molavec.podam
		Junit test para la cración de test de la librería podan para el llenado rápido 
		de atributos.
		
	cl.molavec.jpa
		Junit de operaciones JPA
	
	cl.molavec.qmaker
		JUnit de las operciones de para la generación de cotizaciones.
		
	cl.molavec.reflection
		JUnit de reflexión de los atributos de las entidades. (<-- se utiliza podam para 
		el llenado)
	