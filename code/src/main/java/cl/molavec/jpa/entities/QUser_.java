package cl.molavec.jpa.entities;

import cl.molavec.jpa.entities.QUser;
import cl.molavec.jpa.entities.Quotation;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(QUser.class)
public class QUser_ { 

    public static volatile SingularAttribute<QUser, Integer> id;
    public static volatile SingularAttribute<QUser, String> username;
    public static volatile SingularAttribute<QUser, String> phone;
    public static volatile SingularAttribute<QUser, String> address;
    public static volatile SingularAttribute<QUser, String> email;
    public static volatile SingularAttribute<QUser, String> lastname;
    public static volatile SingularAttribute<QUser, String> firstname;
    public static volatile SingularAttribute<QUser, String> password;
    public static volatile ListAttribute<QUser, Quotation> quotationList;

}