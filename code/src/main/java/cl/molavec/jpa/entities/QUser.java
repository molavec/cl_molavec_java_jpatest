/**
 * int
 * String
 * Quotation
 */

package cl.molavec.jpa.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import uk.co.jemos.podam.annotations.PodamExclude;

@Entity
@Table(name="QUSER")
public class QUser {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@PodamExclude
	private int id;
	
	private String username;
	
	private String password;
	
	private String firstname;
	
	private String lastname;
	
	private String address;
	
	private String phone;
	
	private String email;
	
	@OneToMany(mappedBy="user")
	@PodamExclude
	private List<Quotation> quotationList;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString(){
		return "user: " + this.username;
		
	}
}
