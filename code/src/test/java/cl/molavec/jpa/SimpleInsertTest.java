package cl.molavec.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.molavec.jpa.entities.QUser;


public class SimpleInsertTest {

	private static final String PERSISTENCE_UNIT_NAME = "jpa_test01";
	private static EntityManagerFactory factory;

	private static Logger logger = LoggerFactory.getLogger(SimpleInsertTest.class);

	@Test
    public void insertDummyData() {

		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		// read the existing entries and write to console
		// create new todo
		em.getTransaction().begin();
		QUser user = new QUser();
		user.setUsername("User13");
		em.persist(user);
		em.getTransaction().commit();

		Query q = em.createQuery("select t from QUser t");
		List<QUser> userList = q.getResultList();	    
		for (QUser usern : userList) {
			System.out.println(usern);
		}
		System.out.println("Size: " + userList.size());

		em.close();
		logger.info("------------> End Program");

		factory.close();
		
	}
	
}
