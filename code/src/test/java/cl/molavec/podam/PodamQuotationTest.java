package cl.molavec.podam;

import java.text.SimpleDateFormat;

import javax.persistence.EntityManagerFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import cl.molavec.jpa.entities.Customer;
import cl.molavec.jpa.entities.QUser;
import cl.molavec.jpa.entities.Quotation;


/**
 * Ejemplo de podam:
 * 
 * Id - se excluye
 * Firstname -  podam genera aleatorio, pero se completa manualmente.
 * Lastname - podam genera aleatorio 
 * 
 * @author angel
 *
 */

public class PodamQuotationTest {

	/** The Podam Factory */
    private PodamFactory factory;
    
	private EntityManagerFactory emf;

	@Before
	public void setUp() {
		
		factory = new PodamFactoryImpl();
        Assert.assertNotNull("The PODAM factory cannot be null!", factory);
        Assert.assertNotNull("The factory strategy cannot be null!",
                        factory.getStrategy()); 
        
	}

	@Test
	public void test() {

		Quotation quotation = factory.manufacturePojo(Quotation.class);

		System.out.println("quotation.getId(): " + quotation.getId());
		System.out.println("quotation.getcustomId(): " + quotation.getCustomId());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("quotation.getDate(): " +  format.format(quotation.getDate().getTime()));
		System.out.println("quotation.getState(): " + quotation.getState());
		
		Assert.assertEquals(0, quotation.getId());

	}

}
