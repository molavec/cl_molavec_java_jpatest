package cl.molavec.podam;

import javax.persistence.EntityManagerFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import cl.molavec.jpa.entities.Customer;
import cl.molavec.jpa.entities.QUser;


/**
 * Ejemplo de podam:
 * 
 * Id - se excluye
 * Firstname -  podam genera aleatorio, pero se completa manualmente.
 * Lastname - podam genera aleatorio 
 * 
 * @author angel
 *
 */

public class PodamSimpleTest {

	/** The Podam Factory */
    private PodamFactory factory;
    
	private EntityManagerFactory emf;

	@Before
	public void setUp() {
		
		factory = new PodamFactoryImpl();
        Assert.assertNotNull("The PODAM factory cannot be null!", factory);
        Assert.assertNotNull("The factory strategy cannot be null!",
                        factory.getStrategy()); 
        
	}

	@Test
	public void test() {

		QUser quser = factory.manufacturePojo(QUser.class);
		Customer customer = factory.manufacturePojo(Customer.class);

//		quser.setFirstname("hola"+1);
//		customer.setFirstname("chao"+1);
		
		System.out.println("quser.getId(): " + quser.getId());
		System.out.println("quser.getFirstname(): " + quser.getFirstname());
		System.out.println("quser.getLastname(): " + quser.getLastname());
		

		
		Assert.assertNotEquals(0, quser.getId());

	}

}
