package cl.molavec.compare;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GenerateSchemaTest {

	private static final String PERSISTENCE_UNIT_NAME = "jpa_test01";
	private static EntityManagerFactory factory;

	private static Logger logger = LoggerFactory.getLogger(GenerateSchemaTest.class);

	@Test
    public void generateSchema() {

		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		
		em.close();
		factory.close();
		
		logger.info("------------> End Program");
	}
	
}
