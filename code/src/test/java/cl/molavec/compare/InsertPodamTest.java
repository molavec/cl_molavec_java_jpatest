package cl.molavec.compare;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import cl.molavec.jpa.entities.Company;
import cl.molavec.jpa.entities.Customer;
import cl.molavec.jpa.entities.Item;
import cl.molavec.jpa.entities.MaxFileSizeException;
import cl.molavec.jpa.entities.QUser;
import cl.molavec.jpa.entities.Quotation;
import cl.molavec.jpa.entities.QuotationProperties;

public class InsertPodamTest {

	private static final int QUSERS = 4;
	private static final int CUSTOMERS = 4;
	private static final int QUOTATIONS = 4000;
	private static final int ITEMS = 5;
	/** The Podam Factory */
    private PodamFactory podamf;
    private EntityManagerFactory emf;
    
    private static Logger log = LoggerFactory.getLogger(InsertPodamTest.class);
    
    @Before
    public void setUp() {
    		podamf = new PodamFactoryImpl();
            Assert.assertNotNull("The PODAM factory cannot be null!", podamf);
            Assert.assertNotNull("The factory strategy cannot be null!",
                            podamf.getStrategy());
            
            this.emf = Persistence
    				.createEntityManagerFactory("jpa_test01");
             
    }

    @Test
    public void insertDummyData()  {
    	
    	//Entities
    	QuotationProperties qp = null;
		QUser quser = null;
		Customer customer = null;
		Company company = null;
		Quotation quotation =null;
		
		
		EntityManager em = this.emf.createEntityManager();
		em.getTransaction().begin();
		
		
		try {
			qp = this.quotationPropertiesFactory();
		} catch (IOException | MaxFileSizeException e) {
			Assert.fail(e.getMessage());
		} 
		
		em.persist(qp);
		for(int i=0;i<QUSERS;i++){//Usuarios que generarán cotizaciones
			quser = podamf.manufacturePojo(QUser.class);
			em.persist(quser);
			for(int j=0;j<CUSTOMERS;j++){ //información de a quien va dirigida la cotización
				customer = podamf.manufacturePojo(Customer.class);
	            company = podamf.manufacturePojo(Company.class);
	            em.persist(customer);
	            em.persist(company);
	            for(int k=0;k<QUOTATIONS;k++){ //construcción cotización------------
	            	quotation = podamf.manufacturePojo(Quotation.class);
	            	quotation.setUser(quser);
	            	quotation.setCustomer(customer);
	            	quotation.setCompany(company);
	            	quotation.setQuotationProperties(qp);
	            	List<Item> itemList = new ArrayList<Item>();
	            	for(int l=0;l<ITEMS;l++){ //item de la cotización
	            		Item item = podamf.manufacturePojo(Item.class);
	            		em.persist(item);
	            		itemList.add(item);
	            	}
	            	quotation.setItems(itemList);
	            	em.persist(quotation);
	            }
			}
		}
		
		em.getTransaction().commit();
        em.close();
        Assert.assertNotNull("The pojo cannot be null!", quotation);
        
    }
    
    
    @After
	public void tearDown() throws Exception {
    	emf.close();
	}
    
    /**
     * Genera un obnjeto con propiedades de la cotización.
     * @return
     * @throws MaxFileSizeException 
     * @throws IOException 
     */
    private QuotationProperties quotationPropertiesFactory() throws IOException, MaxFileSizeException {
    	QuotationProperties qp = new QuotationProperties();
    	qp.setLogo("resources"+File.separator+"logo.PNG");
    	qp.setXSLFile("resources"+File.separator+"quotation.xsl");
    	return qp;
    }

}
