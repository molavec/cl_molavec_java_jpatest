package cl.molavec.podam;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import cl.molavec.jpa.entities.Item;

public class PodamItemInsertTest {

	/** The Podam Factory */
    private PodamFactory factory;
    
	private EntityManagerFactory emf;

	@Before
	public void setUp() {
		
		factory = new PodamFactoryImpl();
        Assert.assertNotNull("The PODAM factory cannot be null!", factory);
        Assert.assertNotNull("The factory strategy cannot be null!",
                        factory.getStrategy()); 
        
        this.emf = Persistence
				.createEntityManagerFactory("jpa_test01");
	}

	@Test
	public void test() {

		EntityManager em = this.emf.createEntityManager();

		Item item = factory.manufacturePojo(Item.class);

		System.out.println("item.getId(): " + item.getId());
		
		em.getTransaction().begin();
		em.persist(item);
		System.out.println("item.getId(): " + item.getId());
		em.getTransaction().commit();

		
		Assert.assertNotEquals(0, item.getId());

	}

}
