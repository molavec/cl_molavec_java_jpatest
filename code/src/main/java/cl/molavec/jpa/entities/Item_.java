package cl.molavec.jpa.entities;

import cl.molavec.jpa.entities.Item;
import cl.molavec.jpa.entities.Quotation;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


@StaticMetamodel(Item.class)
public class Item_ { 

    public static volatile SingularAttribute<Item, Integer> id;
    public static volatile SingularAttribute<Item, String> model;
    public static volatile SingularAttribute<Item, Float> price;
    public static volatile ListAttribute<Item, Quotation> quotations;
    public static volatile SingularAttribute<Item, String> description;

}