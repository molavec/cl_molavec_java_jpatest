/**
 * int
 * String
 * float
 * Quotation
 */

package cl.molavec.jpa.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import uk.co.jemos.podam.annotations.PodamExclude;

@Entity
@Table(name="ITEM")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@PodamExclude
	private int id;
	
	private String model;
	
	private String description;
	
	private float price;
	
	@ManyToMany(mappedBy="items")
	@PodamExclude
	private List<Quotation> quotations;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	@Override
	public String toString(){
		return this.getClass().getName() + 
				": [ id: "+ this.getId() +"]"; 
	}
	
}
