package cl.molavec.jpa.entities;

import cl.molavec.jpa.entities.Company;
import cl.molavec.jpa.entities.Customer;
import cl.molavec.jpa.entities.Item;
import cl.molavec.jpa.entities.QUser;
import cl.molavec.jpa.entities.Quotation;
import cl.molavec.jpa.entities.QuotationProperties;
import cl.molavec.jpa.entities.QuotationState;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


@StaticMetamodel(Quotation.class)
public class Quotation_ { 

    public static volatile SingularAttribute<Quotation, Integer> id;
    public static volatile SingularAttribute<Quotation, QuotationProperties> quotationProperties;
    public static volatile SingularAttribute<Quotation, String> customId;
    public static volatile ListAttribute<Quotation, Item> items;
    public static volatile SingularAttribute<Quotation, Company> company;
    public static volatile SingularAttribute<Quotation, QuotationState> state;
    public static volatile SingularAttribute<Quotation, Date> date;
    public static volatile SingularAttribute<Quotation, Customer> customer;
    public static volatile SingularAttribute<Quotation, String> alternativeText;
    public static volatile SingularAttribute<Quotation, QUser> user;

}