package cl.molavec.jpa.entities;

import cl.molavec.jpa.entities.Quotation;
import cl.molavec.jpa.entities.QuotationProperties;
import java.io.File;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


@StaticMetamodel(QuotationProperties.class)
public class QuotationProperties_ { 

    public static volatile SingularAttribute<QuotationProperties, Integer> id;
    public static volatile SingularAttribute<QuotationProperties, File> logo;
    public static volatile ListAttribute<QuotationProperties, Quotation> quotations;
    public static volatile SingularAttribute<QuotationProperties, File> XSLFile;

}