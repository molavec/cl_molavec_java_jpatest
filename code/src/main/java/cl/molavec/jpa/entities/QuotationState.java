package cl.molavec.jpa.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public enum QuotationState {
	PENDING("PENDING"),
	DONE("DONE"),
	SEND("SEND");
	
	static Logger log =LoggerFactory.getLogger(QuotationState.class);
	
	private String id;

	private QuotationState(String id){
		this.id = id;
	}
	
	public String getId(){
		return id;
	}

	public static QuotationState getType(String type) throws Exception {
		for(QuotationState careerKindType: QuotationState.values()){
			if(careerKindType.getId().equals(type)){
				return careerKindType;
			}
		}
		String msg = String.format("Type: %s int doesn't exist in %s",type,QuotationState.class.getName());
		throw new Exception(msg);
	}
		
}
