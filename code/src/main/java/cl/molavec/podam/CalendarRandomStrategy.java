package cl.molavec.podam;


import java.util.Calendar;

import uk.co.jemos.podam.api.AttributeStrategy;
import uk.co.jemos.podam.exceptions.PodamMockeryException;

public class CalendarRandomStrategy implements AttributeStrategy<Calendar>{

	@Override
	public Calendar getValue() throws PodamMockeryException {
		// TODO: generar fechas aleatorias
		
	    Calendar calendar = Calendar.getInstance();

        int year = randBetween(2000, 2010);
        calendar.set(Calendar.YEAR, year);

        int dayOfYear = randBetween(1, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
        calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);

        int hour = randBetween(1, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        
		return calendar;
		
	}
	
	public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

}
