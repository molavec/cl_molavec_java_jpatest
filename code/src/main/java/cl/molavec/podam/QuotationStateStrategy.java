package cl.molavec.podam;

import cl.molavec.jpa.entities.QuotationState;
import uk.co.jemos.podam.api.AttributeStrategy;
import uk.co.jemos.podam.exceptions.PodamMockeryException;


/**
 * Generador automatico estados de cotizaciones
 * 
 * @author angel
 *
 */
public class QuotationStateStrategy implements AttributeStrategy<QuotationState>{

	@Override
	public QuotationState getValue() throws PodamMockeryException {
		// TODO Auto-generated method stub
		return QuotationState.PENDING;
	}

}
