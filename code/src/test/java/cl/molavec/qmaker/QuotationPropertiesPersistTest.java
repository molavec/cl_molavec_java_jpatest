package cl.molavec.qmaker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cl.molavec.jpa.entities.MaxFileSizeException;
import cl.molavec.jpa.entities.QuotationProperties;

public class QuotationPropertiesPersistTest {

	private static final String LOGO_FILE = "resources"+File.separator+"logo.PNG";
	private static final String XSL_FILE = "resources"+File.separator+"quotation.xsl";

	private static final String PERSISTENCE_UNIT_NAME = "jpa_test01";
	private EntityManagerFactory emf;
	
	@Before
	public void setUp(){
		this.emf = Persistence
				.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}

	@After
	public void tearDown(){
		emf.close();
	}

	@Test
	public void test() {
		
		EntityManager em = this.emf.createEntityManager();
		em.getTransaction().begin();
		
		QuotationProperties qp = new QuotationProperties();
		
		
    	try {
			qp.setLogo(LOGO_FILE);
		} catch (IOException | MaxFileSizeException e) {
			Assert.fail(e.getMessage());
		}
    	
		try {
			qp.setXSLFile(XSL_FILE);
		} catch (MaxFileSizeException | IOException e) {
			Assert.fail(e.getMessage());
		}
		
		em.persist(qp);
		
    	try {
    		Assert.assertArrayEquals(IOUtils.toByteArray(new FileInputStream(new File(LOGO_FILE))), qp.getLogo());
    		Assert.assertArrayEquals(IOUtils.toCharArray(new FileInputStream(new File(XSL_FILE))), qp.getXSL());
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		
    	try {
			System.out.println("Extracted logo file from DB generated in... "+qp.getLogoFile().getAbsoluteFile());
			System.out.println("Extracted xsl file from DB generated in... "+qp.getXSLFile().getAbsoluteFile());
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
    	
    	em.getTransaction().commit();
        em.close();
    	
    	
	}

}
