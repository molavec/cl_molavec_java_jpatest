package cl.molavec.podam;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import cl.molavec.jpa.entities.Company;
import cl.molavec.jpa.entities.Customer;
import cl.molavec.jpa.entities.QUser;

public class PodamCompanyInsertTest {

	/** The Podam Factory */
    private PodamFactory factory;
    
	private EntityManagerFactory emf;

	@Before
	public void setUp() {
		
		factory = new PodamFactoryImpl();
        Assert.assertNotNull("The PODAM factory cannot be null!", factory);
        Assert.assertNotNull("The factory strategy cannot be null!",
                        factory.getStrategy()); 
        
        this.emf = Persistence
				.createEntityManagerFactory("jpa_test01");
	}

	@Test
	public void test() {

		EntityManager em = this.emf.createEntityManager();

		Company company = factory.manufacturePojo(Company.class);
		
		em.getTransaction().begin();
		em.persist(company);
		System.out.println("company.getId(): " + company.getId());
		em.getTransaction().commit();
		
		em.close();
		Assert.assertNotEquals(0, company.getId());

	}

}
