package cl.molavec.podam;


/**
 * Clase contenedora de constantes utilizadas para los Generadores
 * 
 * 
 * @author angel
 *
 */
public class MyPodamTestConstants {

	public static int MIN_INT_VALUE = 0;
	public static int MAX_INT_VALUE = 100;
	public static int MIN_FLOAT_VALUE = 0;
	public static int MAX_FLOAT_VALUE = 100;
	
}
