/**
 * int
 * String
 * Date
 */

package cl.molavec.jpa.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cl.molavec.podam.CalendarRandomStrategy;
import cl.molavec.podam.QuotationStateStrategy;
import uk.co.jemos.podam.annotations.PodamExclude;
import uk.co.jemos.podam.annotations.PodamStrategyValue;
import uk.co.jemos.podam.annotations.PodamStringValue;

@Entity
@Table(name="QUOTATION")
public class Quotation {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@PodamExclude
	private int id;
	
	@Temporal(TemporalType.DATE)
	@PodamStrategyValue(CalendarRandomStrategy.class)
	private Calendar date;
	
	private String customId;
	
	@Column(name="STATE", nullable=false)
	@Enumerated(EnumType.STRING)
	@PodamStrategyValue(QuotationStateStrategy.class)
	private QuotationState state;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CUSTOMER_ID", nullable=false)
	@PodamExclude
	private Customer customer;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="QUSER_ID", nullable=false)
	@PodamExclude
	private QUser user;
	
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="QUO_ITEM",
		joinColumns=@JoinColumn(name="QUO_ID"),
		inverseJoinColumns=@JoinColumn(name="ITEM_ID"))
	@PodamExclude
	private List<Item> items;

	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="QPROP_ID", nullable=false)
	@PodamExclude
	private QuotationProperties quotationProperties;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="COMP_ID", nullable=true)
	private Company company;
	
	private String alternativeText;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public String getCustomId() {
		return customId;
	}

	public void setCustomId(String customId) {
		this.customId = customId;
	}

	public QuotationState getState() {
		return state;
	}

	public void setState(QuotationState state) {
		this.state = state;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public QUser getUser() {
		return user;
	}

	public void setUser(QUser user) {
		this.user = user;
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public QuotationProperties getQuotationProperties() {
		return quotationProperties;
	}

	public void setQuotationProperties(QuotationProperties quotationProperties) {
		this.quotationProperties = quotationProperties;
	}

	public String getAlternativeText() {
		return alternativeText;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Company getCompany() {
		return company;
	}

	public void setAlternativeText(String alternativeText) {
		this.alternativeText = alternativeText;
	}
	
	@Override
	public String toString(){
		return "[id: " + this.id + " | state: "+this.getState()+ "]";
	}
	
}
