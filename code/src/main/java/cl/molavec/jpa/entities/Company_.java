package cl.molavec.jpa.entities;

import cl.molavec.jpa.entities.Company;
import cl.molavec.jpa.entities.Quotation;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


@StaticMetamodel(Company.class)
public class Company_ { 

    public static volatile SingularAttribute<Company, Integer> id;
    public static volatile SingularAttribute<Company, String> phone;
    public static volatile SingularAttribute<Company, String> address;
    public static volatile SingularAttribute<Company, String> email;
    public static volatile SingularAttribute<Company, String> companyRut;
    public static volatile SingularAttribute<Company, String> companyName;
    public static volatile ListAttribute<Company, Quotation> quotationList;

}