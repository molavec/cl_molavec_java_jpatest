package cl.molavec.podam;

import java.lang.reflect.Constructor;

import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.dto.AttributeMetadata;


/**
 * Ejemplo de un Gerador completo
 * @author angel
 *
 */
public class MyDataPodamProvider implements DataProviderStrategy{

	@Override
	public Boolean getBoolean(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Byte getByte(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Byte getByteInRange(byte arg0, byte arg1, AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Character getCharacter(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Character getCharacterInRange(char arg0, char arg1,
			AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDouble(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleInRange(double arg0, double arg1,
			AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Float getFloat(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Float getFloatInRange(float arg0, float arg1, AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getInteger(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getIntegerInRange(int arg0, int arg1, AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Long getLong(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getLongInRange(long arg0, long arg1, AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMaxDepth(Class<?> arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getNumberOfCollectionElements(Class<?> arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Short getShort(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Short getShortInRange(short arg0, short arg1, AttributeMetadata arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> Class<? extends T> getSpecificClass(Class<T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStringOfLength(int arg0, AttributeMetadata arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStringValue(AttributeMetadata arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sort(Constructor<?>[] arg0) {
		// TODO Auto-generated method stub
		
	}
	
	

}
