package cl.molavec.jpa.entities;

public class MaxFileSizeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MaxFileSizeException(String msg){
		super(msg);
	}
	
}
