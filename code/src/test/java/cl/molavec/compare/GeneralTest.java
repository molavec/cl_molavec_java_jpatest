package cl.molavec.compare;


import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import cl.molavec.jpa.entities.Company;
import cl.molavec.jpa.entities.Customer;
import cl.molavec.jpa.entities.Item;
import cl.molavec.jpa.entities.MaxFileSizeException;
import cl.molavec.jpa.entities.QUser;
import cl.molavec.jpa.entities.Quotation;
import cl.molavec.jpa.entities.QuotationProperties;
import cl.molavec.jpa.entities.QuotationState;
/**
 * El siguiente test realiza una 
 * 
 *  (nota:
 *  	En cada pausa sugerir el paso del garbage collector
 *      apuntando a null los objetos y utilizando antes de la pausa
 *      
 *      System.gc();
 *      o
 *      Runtime.getRuntime().gc();
 *  )
 *  
 * 1- Select count quotations
 * 
 *  pausa gc
 * 
 * - Select con pagination
 * - Update quotation state page 1
 * - Select con pagination
 * - Update quotation state page 2
 * - Select con pagination
 * - Update quotation state page 3
 * 	
 * 	Pausa
 * - Select count state 1
 * 	- get Item collection y entidades relacionadas
 * - Select count state 2
 * 	- get Item collection y entidades relacionadas
 * - Select count state 3
 * 	- get Item collection y entidades relacionadas
 * 
 *  Pausa
 * - Remove una cotización
 * 
 *  Pausa
 * - Remove varias (todas) cotizaciones
 * 
 * @author angel
 *
 */

public class GeneralTest {

	/** The Podam Factory */
    private PodamFactory podamf;
    private EntityManagerFactory emf;
    
    private static Logger log = LoggerFactory.getLogger(GeneralTest.class);

    private long startTime;
    private long stopTime;
    
    @Before
    public void setUp() {
    		podamf = new PodamFactoryImpl();
            Assert.assertNotNull("The PODAM factory cannot be null!", podamf);
            Assert.assertNotNull("The factory strategy cannot be null!",
                            podamf.getStrategy());
            
            this.emf = Persistence
    				.createEntityManagerFactory("jpa_test01");
            
            this.startTime = 0;
            this.stopTime = 0;
             
    }
    
    @After
	public void tearDown() throws Exception {
    	emf.close();
	}

    @Test
    public void test()  {
    	
    	System.out.println("Porcess: "+ ManagementFactory.getRuntimeMXBean().getName() );

		try {
			Runtime.getRuntime().gc();
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
		
    	
    	EntityManager em = this.emf.createEntityManager();
		
		
    	//1- Select count quotations
    	startTime = System.currentTimeMillis();
		long quotationCount = this.getQuotationCount(em);
		stopTime = System.currentTimeMillis();
		System.out.println("Quotation Quantity = " + quotationCount);
		System.out.println("Count Elapsed time = " + (stopTime -startTime)/1000.0f +"[s]");
		
		try {
			Runtime.getRuntime().gc();
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
		
		
		// - Select con pagination
		startTime = System.currentTimeMillis();
		em.getTransaction().begin();
		List<Quotation> quotationList;
		quotationList = this.selectQuotationPagination(0, (int)quotationCount/3+1, em);
		for (Quotation q : quotationList){
//			System.out.println("1quotation = " + q);
			//q = em.find(Quotation.class, q.getId()); // to get it from actual persisten context
			q.setState(QuotationState.PENDING);
		}
		
		quotationList = this.selectQuotationPagination(1, (int)quotationCount/3+1, em);
		for (Quotation q : quotationList){
//			System.out.println("2quotation = " + q);
			//q = em.find(Quotation.class, q.getId()); // to get it from actual persisten context
			q.setState(QuotationState.DONE);
		}
		
		quotationList = this.selectQuotationPagination(2, (int)quotationCount/3+1, em);
		for (Quotation q : quotationList){
//			System.out.println("3quotation = " + q);
			//q = em.find(Quotation.class, q.getId()); // to get it from actual persisten context
			q.setState(QuotationState.SEND);
		}		
		em.getTransaction().commit();
		stopTime = System.currentTimeMillis();
		System.out.println("Update Elapsed time = " + (stopTime -startTime)/1000.0f +"[s]");
		
		em.close();
		
		try {
			Runtime.getRuntime().gc();
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
		
		
//      Assert.assertNotNull("The pojo cannot be null!", quotation);
        
    }
    
    private long getQuotationCount(EntityManager em){
    	//EntityManager em = this.emf.createEntityManager();
//		em.getTransaction().begin();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Quotation> cqQuotation = cb.createQuery(Quotation.class);
		Root<Quotation> quotationRoot = cqQuotation.from(Quotation.class);
		
		CriteriaQuery<Long> cqLong = cb.createQuery(Long.class);
		cqLong.select(cb.count(quotationRoot));

		TypedQuery<Long> typedQuery = em.createQuery(cqLong);
		long count = typedQuery.getSingleResult();
		
//		em.getTransaction().commit();
        //em.close();
        
    	return count;
    }

    private List<Quotation> selectQuotationPagination(int initial, int pageSize, EntityManager em){
    	List<Quotation> quotationList = new ArrayList<Quotation>();
    	
//    	EntityManager em = this.emf.createEntityManager();
//		em.getTransaction().begin();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Quotation> cqQuotation = cb.createQuery(Quotation.class);
		Root<Quotation> quotationRoot = cqQuotation.from(Quotation.class);
		
		TypedQuery<Quotation> query =  
				em.createQuery(cqQuotation.select(quotationRoot))
					.setFirstResult(initial * pageSize)
					.setMaxResults(pageSize);
		
		quotationList = query.getResultList();
		
//		em.getTransaction().commit();
//      em.close();
		
		
    	
    	return quotationList;
    }
    
    
    
}
