<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="/">
 
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<!-- Formato de la cotización  -->
	<fo:layout-master-set>
		<fo:simple-page-master master-name="quotation"
				page-height="29.7cm"
        		page-width="21cm"
        		margin-top="1cm" margin-bottom="1cm"
          		margin-left="1cm" margin-right="1cm">
			<fo:region-body margin-top="3cm"/>
			<fo:region-before extent="3cm"/>
		</fo:simple-page-master>
	</fo:layout-master-set>

	<!-- Contenido de la cotización  -->
	<fo:page-sequence master-reference="quotation">
	
	<!-- TODO: CAMBIAR POR STYLE  http://www.w3schools.com/xslfo/xslfo_xslt.asp -->
	<fo:static-content flow-name="xsl-region-before">
	<xsl:for-each select="quotation/header_right">
		<fo:block text-align="end" font-size="10pt" font-family="serif"
			line-height="14pt">
       	<xsl:value-of select="."/>
       </fo:block>
       </xsl:for-each>
	</fo:static-content>

		<fo:flow flow-name="xsl-region-body">
			<fo:block>
				Hello W3Schools
			</fo:block>
			<fo:block>
				At W3Schools you will find all the Web-building tutorials you 
		need, from basic HTML and XHTML to advanced XML, XSL, Multimedia and WAP.
			</fo:block>
		</fo:flow>
	</fo:page-sequence>

</fo:root>

</xsl:template>

</xsl:stylesheet>