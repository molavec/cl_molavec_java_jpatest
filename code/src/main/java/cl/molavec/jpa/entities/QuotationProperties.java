/**
 * int
 * File
 */

package cl.molavec.jpa.entities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.io.IOUtils;

import uk.co.jemos.podam.annotations.PodamExclude;

@Entity
@Table(name="QUOTATIONPROPERTIES")
public class QuotationProperties {
	
	private static final int MAX_SIZE_FILE = 512*1024;
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@PodamExclude
	private int id;
	
	@Lob()
	private char[] XSL;
	@Lob()
	private byte[] logo;
	
	@OneToMany(mappedBy="quotationProperties")
	@PodamExclude
	private List<Quotation> quotations;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char[] getXSL() {
		return XSL;
	}

	public void setXSL(char[] XSL) {
		this.XSL = XSL;
	}

	public File getXSLFile() throws IOException {
		File file = new File("resources"+File.separator+"xsl_returned.xsl");
		OutputStream outputStream = new  FileOutputStream(file);
		IOUtils.write(XSL, outputStream);
		outputStream.close();
		return file; 
	}
	
	public void setXSLFile(String path) throws MaxFileSizeException, IOException {
		File file = new File(path);
		InputStream inputStream = new  FileInputStream(file);
		char[] xslCharArray = IOUtils.toCharArray(inputStream); 
		if(xslCharArray.length < MAX_SIZE_FILE){
			this.XSL = xslCharArray;
		}else{
			throw new MaxFileSizeException(
					"xsl bigger than permitted " +
					"("+MAX_SIZE_FILE+") " + xslCharArray.length);
		}
	}
	
	public byte[] getLogo() {
		return logo;
	}
	
	public void setLogo(byte[] logo) {
		this.logo=logo;
	}

	public File getLogoFile() throws IOException {
		File file = new File("resources"+File.separator+"logo_returned.PNG");
		OutputStream outputStream = new  FileOutputStream(file);
		IOUtils.write(logo, outputStream);
		outputStream.close();
		return file; 
	}
	
	public void setLogo(String path) throws IOException, MaxFileSizeException {
		File file = new File(path);
		InputStream inputStream = new  FileInputStream(file);
		byte[] imageByte = IOUtils.toByteArray(inputStream); 
		if(imageByte.length < MAX_SIZE_FILE){
			this.logo = imageByte;
		}else{
			throw new MaxFileSizeException(
					"Image bigger than permitted " +
					"("+MAX_SIZE_FILE+") " + imageByte.length);
		}
	}
	

	
	
	

	
}
