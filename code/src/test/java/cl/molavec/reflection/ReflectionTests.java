package cl.molavec.reflection;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.junit.Test;

import cl.molavec.jpa.entities.QUser;

/**
 * Utilización de org.apache.commons.beanutils.PropertyUtils  
 * 
 * @author angel
 * 
 *
 */

public class ReflectionTests {

	@Test
    public void insertDummyData() throws 
    								IllegalAccessException, 
    								InvocationTargetException, 
    								NoSuchMethodException, 
    								InstantiationException {
		QUser quser =  new QUser();
		quser.setUsername("username1");
		String o1 = (String) PropertyUtils.getSimpleProperty(quser, "username");
		System.out.println(o1);
		
		QUser quser2 =  QUser.class.newInstance();
		quser.setUsername("username2");
		String o2 = (String) PropertyUtils.getSimpleProperty(quser, "username");
		System.out.println(o2);
	}

}
