/**
 * int
 * String
 * Quotation
 */

package cl.molavec.jpa.entities;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import uk.co.jemos.podam.annotations.PodamExclude;

@Entity
@Table(name="COMPANY")
public class Company {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@PodamExclude
	private int id;
	
	private String companyName;
	
	private String address;
	
	private String phone;
	
	private String email;
	
	private String companyRut;
	
	@OneToMany(mappedBy="company")
	@PodamExclude
	private List<Quotation> quotationList;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyRut() {
		return companyRut;
	}

	public void setCompanyRut(String companyRut) {
		this.companyRut = companyRut;
	}
}
